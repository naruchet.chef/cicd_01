package dto

type Todo struct {
	Title  string `json:"title"`
	Status string `json:"status"`
}
