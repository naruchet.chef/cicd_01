package repository

import "lab42/domain/dao"

type TodoRepository interface {
	Create(id, name, status string) error
	GetAll() ([]dao.Todo, error)
}

type TodoRepositoryImp struct {
}

func InitTodoRepository() *TodoRepositoryImp {
	return &TodoRepositoryImp{}
}

func (r *TodoRepositoryImp) Create(id, name, status string) error {
	dao.Todos = append(dao.Todos, dao.Todo{
		ID:     id,
		Title:  name,
		Status: status,
	})
	return nil
}

func (r *TodoRepositoryImp) GetAll() ([]dao.Todo, error) {
	return dao.Todos, nil
}
