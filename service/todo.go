package service

import (
	"lab42/domain/dto"
	"lab42/repository"
)

// port of handler/todo.go
type TodoService interface {
	CreateTodo(id, name, status string) error
	GetTodos() ([]dto.Todo, error)
}

// adapter of handler/todo.go
type TodoServiceImp struct {
	repo repository.TodoRepository
}

func InitTodoService(repo repository.TodoRepository) *TodoServiceImp {
	return &TodoServiceImp{
		repo: repo,
	}
}

func (s *TodoServiceImp) CreateTodo(id, name, status string) error {
	s.repo.Create(id, name, status)
	return nil
}

func (s *TodoServiceImp) GetTodos() ([]dto.Todo, error) {
	list, err := s.repo.GetAll()
	if err != nil {
		return nil, err
	}
	res := []dto.Todo{}
	for _, v := range list {
		res = append(res, dto.Todo{
			Title:  v.Title,
			Status: v.Status,
		})
	}
	return res, nil
}
