package service

import (
	"errors"
	"lab42/domain/dao"
	"testing"
)

type MockTodoRepository struct {
	err error
}

func (m *MockTodoRepository) Create(id, name, status string) error {
	return m.err
}

func (m *MockTodoRepository) GetAll() ([]dao.Todo, error) {
	return []dao.Todo{
		{
			ID:     "1",
			Title:  "test",
			Status: "active",
		},
	}, nil
}

func TestCreateTodo(t *testing.T) {
	t.Run("test create todo success", func(t *testing.T) {
		mockRepo := &MockTodoRepository{}
		s := InitTodoService(mockRepo)
		err := s.repo.Create("1", "test", "active")
		if err != nil {
			t.Errorf("error should be nil")
		}
	})

	t.Run("test create todo failed", func(t *testing.T) {
		mockRepo := &MockTodoRepository{
			err: errors.New("server error"),
		}
		s := InitTodoService(mockRepo)
		err := s.repo.Create("1", "test", "active")
		if err == nil {
			t.Errorf("error should not be nil")
		}
	})

	t.Run("test create todo", func(t *testing.T) {
		scenarios := []struct {
			Id            string
			Name          string
			Status        string
			ExpectedError error
		}{
			{
				Id:            "1",
				Name:          "test",
				Status:        "active",
				ExpectedError: nil,
			}, {
				Id:            "1",
				Name:          "test",
				Status:        "active",
				ExpectedError: errors.New("server error"),
			},
		}

		for _, sc := range scenarios {
			mockRepo := &MockTodoRepository{
				err: sc.ExpectedError,
			}
			s := InitTodoService(mockRepo)
			err := s.repo.Create(sc.Id, sc.Name, sc.Status)
			if err != sc.ExpectedError {
				t.Errorf("error should be nil")
			}
		}
	})

}
