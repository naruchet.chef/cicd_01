# ใช้ Go 1.16 ในการสร้าง Image
FROM golang:1.20

# กำหนด Directory ที่จะเป็นที่ทำงานใน Container
WORKDIR /app

# คัดลอกโค้ดของแอปพลิเคชัน Go เข้าสู่ Container
COPY . .

# Build แอปพลิเคชัน Go
RUN go build -o main .

# กำหนดพอร์ตที่แอปพลิเคชันจะใช้ใน Container
EXPOSE 8080

# คำสั่งที่จะถูกเรียกใช้เมื่อ Container ถูกเริ่มต้น
CMD ["./main"]
