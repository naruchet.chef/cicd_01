package handler

import (
	"lab42/domain/dao"
	"lab42/service"

	"github.com/gin-gonic/gin"
)

type TodoHandler struct {
	todoService service.TodoService
}

func InitTodoHandler(todoService service.TodoService) *TodoHandler {
	return &TodoHandler{
		todoService: todoService,
	}
}

func (h *TodoHandler) CreateTodo(c *gin.Context) {
	todo := dao.Todo{}
	c.BindJSON(&todo)
	h.todoService.CreateTodo(todo.ID, todo.Title, todo.Status)
	c.JSON(200, gin.H{
		"message": "success",
	})
}

func (h *TodoHandler) GetTodos(c *gin.Context) {
	res, err := h.todoService.GetTodos()
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, gin.H{
		"message": "success",
		"data":    res,
	})
}
