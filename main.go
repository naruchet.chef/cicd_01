package main

import (
	"lab42/handler"
	"lab42/repository"
	"lab42/service"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()

	todoRepository := repository.InitTodoRepository()
	todoService := service.InitTodoService(todoRepository)
	todoHandler := handler.InitTodoHandler(todoService)

	r.POST("/todos", todoHandler.CreateTodo)
	r.GET("/todos", todoHandler.GetTodos)

	r.Run()
}
